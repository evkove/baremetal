#include "stm32f4xx.h"

void gpio_init()
{
    //PD5
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    GPIOD->MODER |= GPIO_MODER_MODER3_0;
    GPIOB->MODER |= GPIO_MODER_MODER15_0;
    GPIOG->MODER |= GPIO_MODER_MODER10_0;
    GPIOG->MODER &= ~GPIO_MODER_MODER11;
    //OTYPER keep reset value

    GPIOD->OSPEEDR |= GPIO_OSPEEDR_OSPEED11; //0b11
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEED13; //0b11
    GPIOG->OSPEEDR |= GPIO_OSPEEDR_OSPEED8; //0b11
    //PUPDR keep reset value
}
void delay_ms(int ms)
{
    for(int i = 0; i < 1000 * ms; i++)
    {
        asm("nop");
    }
}

void send(int message)
{
	double dl = 0.9;
	GPIOG->BSRR ^= GPIO_BSRR_BR_10; //SCK
        delay_ms(dl);
        GPIOD->BSRR ^= GPIO_BSRR_BR_3;  //CLKRES
        delay_ms(dl); //that's will not work dl is double delay_ms(INT)
                      //dl will be casted to int. so it will be as delay_ms(0)
for(int i=0; i<16; i++)
{
    GPIOD->BSRR ^= GPIO_BSRR_BR_3;  //CLKRES
        delay_ms(dl);

        if(message & 0b1)
        {

            GPIOB->BSRR ^= GPIO_BSRR_BS_15; //data_on
            delay_ms(dl);
        }
        else
         {
            GPIOB->BSRR ^= GPIO_BSRR_BR_15; //data_off
           delay_ms(dl);
         }
    GPIOD->BSRR ^= GPIO_BSRR_BS_3;  //CLKSET
        delay_ms(dl);
	message = message>>1;// you can type as message >>= 1; it's not a mistake but less code less errors

}

	GPIOG->BSRR ^= GPIO_BSRR_BS_10; //SCK
	delay_ms(dl);
	}

int main(void)
{
    gpio_init();
    		
    while(1)
    {
	send(0b0000000000100000);		//check btn1
        if(!(GPIOG->IDR & GPIO_IDR_IDR_11))
		send(0b0010000100000010);      //show number1

        send(0b0000000001000000);              // check btn2
        if(!(GPIOG->IDR & GPIO_IDR_IDR_11))
		send(0b0001010100011001);      // show number2

        send(0b0000000010000000);              // check btn3
        if(!(GPIOG->IDR & GPIO_IDR_IDR_11))
                send(0b0001001100001011);      // show number3
       
       	send(0b1000000000000000);             // check btn4
        if(!(GPIOG->IDR & GPIO_IDR_IDR_11))
                send(0b0100100100000011);     // show number4
	
    }
}